<?php

use Illuminate\Http\Request;
// use Illuminate\Routing\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


//Auth
Route::post('login', 'JWTAuthController@login');
Route::post('register', 'JWTAuthController@register');
Route::post('logout', 'JWTAuthController@expireToken');
Route::post('refresh', 'JWTAuthController@refreshToken');

//Articles
Route::get('articles', 'ArticleController@index');
Route::post('articles', 'ArticleController@store');
Route::get('article/{article}', 'ArticleController@show');
Route::put('article/{article}', 'ArticleController@update');
Route::delete('article/{article}', 'ArticleController@destroy');