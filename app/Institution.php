<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

use app\User;
class Institution extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email'
    ];

    public function user ()
    {
        return $this->hasOne(User::class);
    }
    
}
