<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\MainController;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Auth;

class JWTAuthController extends MainController
{
    
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if(! $token = $this->guard()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }


    public function register(RegisterRequest $request)
    {

        $user = User::create($request->all());
    }

    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'toke_type' => 'bearer',
            // 'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    public function guard()
    {
        return Auth::guard();
    }
}
